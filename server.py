"""
Our main server file that incorporates our routes and database calls, as well as initiates flask.
"""

# libraries
from os import environ as env
from urllib.parse import quote_plus, urlencode

import sqlite3
from datetime import datetime

from authlib.integrations.flask_client import OAuth
from dotenv import find_dotenv, load_dotenv
from flask import Flask, redirect, render_template, session, url_for, jsonify, request

# load .env file
ENV_FILE = find_dotenv()
if ENV_FILE:
    load_dotenv(ENV_FILE)

# configure Flask
app = Flask(__name__)
app.secret_key = env.get("APP_SECRET_KEY")

#sqlite connection
def get_db_connection():
    """Initial database connection used throughout routes"""
    connection = sqlite3.connect('database.db')
    connection.row_factory = sqlite3.Row
    return connection

# configure Authlib to handle authentification with Auth0
oauth = OAuth(app)

oauth.register(
    "auth0",
    client_id=env.get("AUTH0_CLIENT_ID"),
    client_secret=env.get("AUTH0_CLIENT_SECRET"),
    client_kwargs={
        "scope": "openid profile email",
    },
    server_metadata_url=f'https://{env.get("AUTH0_DOMAIN")}/.well-known/openid-configuration'
)

@app.route("/login")
def login():
    """Triggering authentification with /login"""
    return oauth.auth0.authorize_redirect(
        redirect_uri=url_for("callback", _external=True)
    )

@app.route("/callback", methods=["GET", "POST"])
def callback():
    """Finalizing authentification with /callback"""
    token = oauth.auth0.authorize_access_token()
    session["user"] = token
    return redirect("/")

@app.route("/logout")
def logout():
    """Clearing a session with /logout"""
    session.clear()
    return redirect(
        "https://" + env.get("AUTH0_DOMAIN")
        + "/v2/logout?"
        + urlencode(
            {
                "returnTo": url_for("home", _external=True),
                "client_id": env.get("AUTH0_CLIENT_ID"),
            },
            quote_via=quote_plus,
        )
    )

def url_tag(url):
    """
    Function to split a url to get the domain for use in the HTML tag
    """
    return url.split('/')[2]

def time_format(time):
    """
    Formats the unix timestamp of an article
    """
    return datetime.fromtimestamp(time)

def retrieve_info(article_id):
    """
    Fetches specific article from the database to view its information
    """
    connection = get_db_connection()
    article = connection.execute(
        'SELECT * FROM articles WHERE id = ?',
        (article_id,)
        ).fetchone()
    return article

@app.route("/")
def home():
    """
    Route for home page, main database calls for displaying all articles,
    and logic for selecting which articles to keep liked or disliked
    """

    if session.get('user'):
        connection = get_db_connection()
        stories = connection.execute(
            'SELECT * FROM articles ORDER BY created DESC limit 30'
            ).fetchall()
        liked_articles = connection.execute(
            'SELECT articleID FROM likes WHERE email = ?',
            (session.get('user')['userinfo']['email'],)
            ).fetchall()
        disliked_articles = connection.execute(
            'SELECT articleID FROM dislikes WHERE email = ?',
            (session.get('user')['userinfo']['email'],)
            ).fetchall()
        connection.close()
        likes = []
        dislikes = []
        for article in liked_articles:
            likes.append(article['articleID'])
        for article in disliked_articles:
            dislikes.append(article['articleID'])
        return render_template(
            "home.html",
            session=session.get('user'),
            stories=stories,
            likes=likes,
            dislikes=dislikes,
            urlTag=url_tag,
            timeFormat=time_format,
            indent=4
            )

    return render_template(
        "home.html",
        session=session.get('user')
        )

@app.route("/profile")
def profile():
    """
    Route to view profile page, and database logic for selecting which stories to display
    """
    if session.get('user'):
        connection = get_db_connection()
        profile_likes = connection.execute(
            'SELECT * FROM likes WHERE email = ?',
            (session.get('user')['userinfo']['email'],)
            ).fetchall()
        profile_dislikes = connection.execute(
            'SELECT * FROM dislikes WHERE email = ?',
            (session.get('user')['userinfo']['email'],)
            ).fetchall()
        connection.close()
        formatted_likes = []
        formatted_dislikes = []
        for profile_like in profile_likes:
            formatted_likes.append(retrieve_info(profile_like['articleID']))
        for profile_dislike in profile_dislikes:
            formatted_dislikes.append(retrieve_info(profile_dislike['articleID']))
        return render_template(
            "profile.html",
            session=session.get('user'),
            likes=formatted_likes,
            dislikes=formatted_dislikes,
            urlTag=url_tag,
            timeFormat=time_format
            )

    return render_template(
        "profile.html",
        session=session.get('user')
        )

@app.route("/admin", methods=['GET', 'POST'])
def admin():
    """
    Route for the admin page and logic for viewing and displaying correct likes/dislikes
    """

    permitted = ['elijahbarrios@gmail.com',
                'noahromo.cs@gmail.com',
                'chashimahiulislam@gmail.com',
                'piyush@compgeom.com'
                ]

    if session.get('user'):

        email = request.form.get("userDropdown")

        connection = get_db_connection()
        stories = connection.execute('SELECT * FROM articles WHERE likes != 0').fetchall()
        users = connection.execute('SELECT DISTINCT email FROM likes').fetchall()
        likes = connection.execute(
            'SELECT * FROM likes WHERE email = ?',
            (email,)
            ).fetchall()
        dislikes = connection.execute(
            'SELECT * FROM dislikes WHERE email = ?',
            (email,)
            ).fetchall()
        connection.close()

        return render_template(
            "admin.html",
            session=session.get('user'),
            stories=stories,
            users=users,
            permitted=permitted,
            likes=likes,
            dislikes=dislikes,
            retrieveInfo=retrieve_info,
            timeFormat=time_format,
            email=email
            )

    return render_template(
        "admin.html",
        session=session.get('user'),
        permitted=permitted
        )

@app.route("/about")
def about():
    """Route for the about page"""
    return render_template("about.html")

@app.route("/like-story/<article_id>", methods=['POST'])
def like(article_id):
    """Route for a user to like a story and alter the database"""

    connection = get_db_connection()

    # checking if story exists
    story = connection.execute(
        'SELECT * FROM articles WHERE id = ?',
        (article_id,)
        ).fetchone()

    # checking if user has already liked
    like_hasliked = connection.execute(
        'SELECT * FROM likes WHERE email = ? AND articleID = ?',
        (session.get('user')['userinfo']['email'], article_id)
        ).fetchone()
    like_hasdisliked = connection.execute(
        'SELECT * FROM dislikes WHERE email = ? AND articleID = ?',
        (session.get('user')['userinfo']['email'], article_id)
        ).fetchone()

    if story is None:
        return jsonify({'error': 'Story does not exist.'}, 400)
    if like_hasliked:
        connection.execute(
            'DELETE FROM likes WHERE email = ? AND articleID = ?',
            (session.get('user')['userinfo']['email'], article_id)
            )
        connection.execute(
            'UPDATE articles SET likes = likes-1 WHERE id = ?',
            (article_id,)
            )
        connection.commit()
    elif like_hasdisliked:
        connection.execute(
            'DELETE FROM dislikes WHERE email = ? AND articleID = ?',
            (session.get('user')['userinfo']['email'], article_id)
            )
        connection.execute(
            'UPDATE articles SET dislikes = dislikes-1 WHERE id = ?',
            (article_id,)
            )
        connection.execute(
            'INSERT INTO likes (email, articleID) VALUES (?, ?)',
            (session.get('user')['userinfo']['email'], article_id)
            )
        connection.execute(
            'UPDATE articles SET likes = likes+1 WHERE id = ?',
            (article_id,)
            )
        connection.commit()
    else:
        connection.execute(
            'INSERT INTO likes (email, articleID) VALUES (?, ?)',
            (session.get('user')['userinfo']['email'], article_id)
            )
        connection.execute(
            'UPDATE articles SET likes = likes+1 WHERE id = ?',
            (article_id,)
            )
        connection.commit()

    connection.close()
    if like_hasliked is None:
        if like_hasdisliked is None:
            return jsonify(liked=0, disliked=0)
        return jsonify(liked=0, disliked=1)
    if like_hasdisliked is None:
        return jsonify(liked=1, disliked=0)
    return jsonify(liked=1, disliked=1)

@app.route("/dislike-story/<article_id>", methods=['POST'])
def dislike(article_id):
    """Route for a user to dislike a story and alter the database"""

    connection = get_db_connection()

    # checking if story exists
    story = connection.execute(
        'SELECT * FROM articles WHERE id = ?',
        (article_id,)
        ).fetchone()

    # checking if user has already disliked or liked
    dislike_hasdisliked = connection.execute(
        'SELECT * FROM dislikes WHERE email = ? AND articleID = ?',
        (session.get('user')['userinfo']['email'], article_id)
        ).fetchone()
    dislike_hasliked = connection.execute(
        'SELECT * FROM likes WHERE email = ? AND articleID = ?',
        (session.get('user')['userinfo']['email'], article_id)
        ).fetchone()

    if story is None:
        return jsonify({'error': 'Story does not exist.'}, 400)
    if dislike_hasdisliked:
        connection.execute(
            'DELETE FROM dislikes WHERE email = ? AND articleID = ?',
            (session.get('user')['userinfo']['email'], article_id)
            )
        connection.execute(
            'UPDATE articles SET dislikes = dislikes-1 WHERE id = ?',
            (article_id,)
            )
        connection.commit()
    elif dislike_hasliked:
        connection.execute(
            'DELETE FROM likes WHERE email = ? AND articleID = ?',
            (session.get('user')['userinfo']['email'], article_id)
            )
        connection.execute(
            'UPDATE articles SET likes = likes-1 WHERE id = ?',
            (article_id,)
            )
        connection.execute(
            'INSERT INTO dislikes (email, articleID) VALUES (?, ?)',
            (session.get('user')['userinfo']['email'], article_id)
            )
        connection.execute(
            'UPDATE articles SET dislikes = dislikes+1 WHERE id = ?',
            (article_id,)
            )
        connection.commit()
    else:
        connection.execute(
            'INSERT INTO dislikes (email, articleID) VALUES (?, ?)',
            (session.get('user')['userinfo']['email'], article_id)
            )
        connection.execute(
            'UPDATE articles SET dislikes = dislikes+1 WHERE id = ?',
            (article_id,)
            )
        connection.commit()

    connection.close()
    if dislike_hasdisliked is None:
        if dislike_hasliked is None:
            return jsonify(liked=0, disliked=0)
        return jsonify(liked=1, disliked=0)
    if dislike_hasliked is None:
        return jsonify(liked=0, disliked=1)
    return jsonify(liked=1, disliked=1)

@app.route("/delete-post/<article_id>", methods=['POST'])
def delete_post(article_id):
    """
    This is the route for deleting an article from our database, as requested in the admin tab
    """

    email = request.args.get('user')
    connection = get_db_connection()
    story = connection.execute(
        'SELECT * FROM articles WHERE id = ?', (article_id,)
        ).fetchone()
    selected_like = connection.execute(
        'SELECT * FROM likes WHERE email = ? AND articleID = ?', (email, article_id)
        ).fetchone()
    selected_dislike = connection.execute(
        'SELECT * FROM dislikes WHERE email = ? AND articleID = ?', (email, article_id)
        ).fetchone()

    if story is None:
        return jsonify({'error': 'Story does not exist.'}, 400)
    if selected_like:
        connection.execute('DELETE FROM likes WHERE email = ? AND articleID = ?',
            (email, article_id)
            )
        connection.execute('UPDATE articles SET likes = likes-1 WHERE id = ?',
            (article_id,)
            )
        connection.commit()
    elif selected_dislike:
        connection.execute('DELETE FROM dislikes WHERE email = ? AND articleID = ?',
            (email, article_id)
            )
        connection.execute('UPDATE articles SET dislikes = dislikes-1 WHERE id = ?',
            (article_id,)
            )
        connection.commit()

    connection.close()
    return 'Success'

# server instantiation
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=env.get("PORT", 5000))
