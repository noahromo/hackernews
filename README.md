# **Noah and Elijah's Hacker News**

## Getting Started

If you would like to visit the website, you can click this link here [hackernews](https://elijahandnoahnews.lol/)

To see the website on a local server follow these steps (It is recommended to use the VSCode IDE).

First, clone the repository onto your local machine:

```bash
$ git clone https://gitlab.com/elibarrios/hackernews.git
```

Now follow the installation and usage steps below!

## Installation

Make sure you have python3, pip, and virtualenv installed

Create and activate virtual environment using virtualenv:

```bash
$ virtualenv env
$ source env/bin/activate
```

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install all dependencies!

```bash
pip install -r requirements.txt
```

## Usage

Now check out the main branch if you aren’t already on it:
```bash
$ git checkout main
```

Create a .env file and add the variables from your project settings in Auth0.

Start the Gunicorn server:
```bash
$ gunicorn --bind 0.0.0.0:5000 wsgi:app
```

You should get a response like this in the terminal:
```
[2022-11-03 11:57:50 -0400] [47217] [INFO] Starting gunicorn 20.1.0
[2022-11-03 11:57:50 -0400] [47217] [INFO] Listening at: http://0.0.0.0:5000 (47217)
[2022-11-03 11:57:50 -0400] [47217] [INFO] Using worker: sync
[2022-11-03 11:57:51 -0400] [47223] [INFO] Booting worker with pid: 47223
```

You'll now be able to access the website at `localhost:5000` or `127.0.0.1:5000` in the browser! 

*Note: The portfolio site will only work on your local machine while you have it running inside of your terminal. It is hosted on the cloud for all to see at our URL at the top!* 

## Preface - Homework 1

*COP 4521, Group ID 10*

Elijah Barrios - ebb20b

Noah Romo - ner20

---

How the command "curl http://elijahandnoahnews.lol:3000/" works:

![](Curl.png)

Curl first figures out the IP address of the host it wants to communicate with, then connects to it. "Connecting to it" means performing a TCP protocol handshake.

Curl also supports connections using the HTTP protocol, which has a default port of 80, which is the port being used by our current website.

The output of our curl command is: 
```bash
<html>
  <head>
    <meta charset="utf-8" />
    <title>Auth0 Example</title>
  </head>
  <body>
    
    <h1>Welcome Guest</h1>
    <p><a href="/login">Login</a></p>
    
  </body>
</html>
```

**Security**

Our ssh setup allows only users who have had their public key added to our configuration, and no passwords are used for connecting. Therefore, one cannot simply guess a password to connect to one of the users of our server. The only way to connect to our ssh server is by using one of the devices that have their public keys shared, as we will not be sharing or distributing our public keys to allow for anyone else to have this unique code.

We will generate an SSL certificate with certbot to ensure that our web server is using HTTPS instead of HTTP. This ensures that traffic sent is encrypted and not just pure plaintext. 

The Ubuntu server comes with its own firewall known as ufw, or Uncomplicated Firewall. This allows us to control traffic along specific ports, and keep out specific intruders or exclude any range of IP's that we do not want interacting with our server.
