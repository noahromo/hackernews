function like(id) {
    const likeButton = document.getElementById(`like-button-${id}`)
    const dislikeButton = document.getElementById(`dislike-button-${id}`)

    fetch(`/like-story/${id}`, {method: "POST"})
    .then((res) => res.json())
    .then((data) => {
        if(data["liked"] == 0) {
            if(data["disliked"] == 0) {
                likeButton.className = "button is-medium is-success"
            } else if(data["disliked"] == 1) {
                likeButton.className = "button is-medium is-success"
                dislikeButton.className = "button is-medium"
            }
        } else if(data["liked"] == 1) {
            likeButton.className = "button is-medium"
        }
    })
    .catch((e) => alert("Could not like post."))
}

function dislike(id) {
    const likeButton = document.getElementById(`like-button-${id}`)
    const dislikeButton = document.getElementById(`dislike-button-${id}`)

    fetch(`/dislike-story/${id}`, {method: "POST"})
    .then((res) => res.json())
    .then((data) => {
        if(data["disliked"] == 0) {
            if(data["liked"] == 0) {
                dislikeButton.className = "button is-medium is-danger"
            } else if(data["liked"] == 1)
                dislikeButton.className = "button is-medium is-danger"
                likeButton.className = "button is-medium"
        } else if(data["disliked"] == 1) {
            dislikeButton.className = "button is-medium"
        }
    })
    .catch((e) => alert("Could not dislike post."))
}
