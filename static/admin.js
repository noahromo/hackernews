function deletePost(id, email) {
    fetch(`/delete-post/${id}?user=${email}`, {method: "POST"})
    .then(() => {
        document.getElementById(`${id}`).remove()
    })
    .catch((e) => alert("Could not delete post." + e))
}