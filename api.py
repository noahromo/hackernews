# pylint: disable=consider-using-f-string
#!/usr/bin/env python3
"""
Crontab runs this file every hour to gather articles and add them to the database.
"""

import sqlite3
import asyncio
import httpx

connection = sqlite3.connect(
        '/home/projectShared/hackernews/database.db'
        )

with open('/home/projectShared/hackernews/schema.sql', encoding='utf-8') as f:
    connection.executescript(f.read())

cur = connection.cursor()

# Hackernews API Call

URL = 'https://hacker-news.firebaseio.com/v0/item/{}.json'

async def get_articles():
    """This function gathers articles and executes database calls to insert unique ones"""
    async with httpx.AsyncClient() as sync_call:
        new = (await sync_call.get('https://hacker-news.firebaseio.com/v0/newstories.json')).json()
        tasks = [sync_call.get(URL.format(article)) for article in new[0:50]]
        stories = await asyncio.gather(*tasks)
        for story in stories:
            if story.json():
                if story.json().get('url'):
                    cur.execute(
                        'INSERT or IGNORE INTO articles({}, {}, {}, {}) VALUES (?, ?, ?, ?)'.format(
                            'id',
                            'title',
                            'content',
                            'created'
                        ),
                        (story.json()["id"],
                        story.json()["title"],
                        story.json()["url"],
                        story.json()["time"])
                    )

if __name__ == "__main__":
    asyncio.run(get_articles())

connection.commit()
connection.close()
